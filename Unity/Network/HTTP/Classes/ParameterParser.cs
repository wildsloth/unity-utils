﻿using UnityEngine;
using System.Collections;

namespace OpenUtils.Network
{
    using System;
    using System.Text.RegularExpressions;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Splits URI Parameter List into dictionary with key and value
    /// </summary>
    public class ParameterParser : IConverter<Dictionary<string, string>>
    {
        public Dictionary<string, string> lastResult { get; private set; }
        public Dictionary<string, string> Parse(string uri)
        {
            var matches = Regex.Matches(uri, @"[\?&](([^&=]+)=([^&=#]*))");
            Dictionary<string, string> result = matches.Cast<Match>().ToDictionary(
                m => Uri.UnescapeDataString(m.Groups[2].Value),
                m => Uri.UnescapeDataString(m.Groups[3].Value)
            );
            lastResult = result;
            return result;
        }
    }

}
