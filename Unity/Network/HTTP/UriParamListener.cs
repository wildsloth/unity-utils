﻿using UnityEngine;
using System.Collections;

namespace OpenUtils.Network
{
    using System;
    using System.Collections.Generic;

    public class UriParamEventArgs : EventArgs
    {
        public Dictionary<string, string> uriParams { get; private set; }

        public UriParamEventArgs(Dictionary<string, string> dict)
        {
            this.uriParams = dict;
        }
    }

    [RequireComponent(typeof(WebserverComponent))]
    public class UriParamListener : MonoBehaviour
    {
        public event EventHandler<UriParamEventArgs> Received;

        private WebserverComponent serverRef;
        private IConverter<Dictionary<string, string>> parser;

        void Start()
        {
            parser = new ParameterParser();
        }

        void OnEnable()
        {
            if (serverRef == null)
                serverRef = GetComponent<WebserverComponent>();
            if (serverRef != null)
                serverRef.Connection += OnConnection;
        }

        void OnDisable()
        {
            serverRef.Connection -= OnConnection;
        }

        private void OnConnection(object sender, HttpEventArgs args)
        {
            if (args.request != null)
            {
                string uri = args.request.Url.ToString();
                Dictionary<string, string> result = parser.Parse(uri);


                if (result.Count > 0)
                {
                    if (Received != null)
                    {
                        Received(this, new UriParamEventArgs(result));
                    }
                }
            }

        }
    }
}

