﻿using System;
using System.Net;
using System.Collections.Generic;
using UnityEngine;

namespace OpenUtils.Network
{
    public class WebserverComponent : MonoBehaviour
    {
        //public static TextAsset textfile;
        public string _url = "http://localhost:8080/unity/";
        public string URL
        {
            get
            {
                return _url;
            }
            set
            {
                if(server != null)
                {
                    Debug.Log("restarting server " + _url + " -> " + value);
                    server.Stop();
                }

                _url = value;

                requests = new List<HttpListenerRequest>();
                Application.runInBackground = true;
                server = new WebServer(SendResponse, _url);
                server.Run();
            }
        }

        // raised when new connection happens:
        public event EventHandler<HttpEventArgs> Connection;

        private WebServer server;
        private static List<HttpListenerRequest> requests;
        
        void Start()
        {
            RestartServer();
        }

        public void RestartServer()
        {
            Debug.Log("restarting server " + URL);
            if (server != null)
            {
                server.Stop();
            }
            
            requests = new List<HttpListenerRequest>();
            Application.runInBackground = true;
            server = new WebServer(SendResponse, URL);
            server.Run();
        }

        private static string LogHistory;

        void Update()
        {
            if(requests != null)
            {
                if (requests.Count > 0)
                {
                    for (int i = 0; i < requests.Count; i++)
                    {
                        HttpListenerRequest request = requests[i];
                        if (Connection != null)
                            Connection(null, new HttpEventArgs(request));
                    }
                    requests.Clear();
                }
            }
        }

        public static string SendResponse(HttpListenerRequest r)
        {
            if (r != null)
            {
                requests.Add(r);
            }

            string body = "Log";
            /*IEnumerable<Debug.LogEntry> logs = Debug.GetLogs();
            foreach (Debug.LogEntry log in logs)
            {
                body += log.message + "\n";
            }*/

            //string html = "<html> <head> <style> #main { position: absolute; width: 800px; left: 50%; margin-left: -450px; background: #fff; padding: 100px; } #main a { display: inline-block; text-decoration: none; border-radius: 4px; padding: 10px; border: 1px solid #ddd; background-color: #eee; } body { background: rgb(100, 20, 240); font-size: 20px; } </style> </head> <body> <div id=\"main\"> <a href=\"?hi\">say hello</a> <a href=\"?goodbye\">say bye</a> </div> </body> </html>";
            return body;
        }

        public void OnDestroy()
        {
            server.Stop();
        }
    }

}