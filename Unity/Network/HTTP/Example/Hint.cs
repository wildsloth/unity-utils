﻿using UnityEngine;
using System.Collections;
using OpenUtils.Network;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Hint : MonoBehaviour
{
    public WebserverComponent server;

    void Start()
    {
        if(server != null)
        {
            Text t = GetComponent<Text>();
            t.text = "server url " + server.URL;
        }
    }
}
