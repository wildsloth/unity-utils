﻿using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
using OpenUtils.Json;

namespace OpenUtils.Log
{
    public interface ILogWriter
    {
        void Write(string message, string stackTrace, LogType type);
    }

    public enum LogWriterTypes
    {
        TextFile,
        Json
    }

    public class TextWriter : ILogWriter
    {
        public string path;
        public WriterSettings settings;

        public TextWriter(string path, WriterSettings settings)
        {
            this.path = path;
            this.settings = settings;
        }

        public void Write(string message, string stackTrace, LogType type)
        {
            if (settings.DoLog(type))
            {
                using (StreamWriter sr = new StreamWriter(path, true))
                {
                    string time = DateTime.Now.Timestamp();
                    sr.WriteLine(time + "\n");
                    sr.WriteLine(message + "\n");
                    if (settings.LogStackTrace(type))
                        sr.WriteLine(stackTrace + "\n");

                    sr.WriteLine("\n");

                    sr.Close();
                }
            }
        }
    }
    public class JsonWriter : ILogWriter
    {
        Writer writer;
        Reader reader;
        WriterSettings settings;
        string path;

        List<LogElement> logs;

        public JsonWriter(string path, WriterSettings settings)
        {
            this.path = path;
            this.settings = settings;

            reader = new Reader();
            writer = new Writer(new Newtonsoft.Json.JsonSerializerSettings()
            {
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
            });

            // potentially change this to save the list of logs
            logs = reader.Read<List<LogElement>>(path);
            if (logs == null)
                logs = new List<LogElement>();
        }

        public void Write(string message, string stackTrace, LogType type)
        {
            if (settings.DoLog(type))
            {
                logs.Add(new LogElement()
                {
                    message = message,
                    type = type.ToCleanString(),
                    time = DateTime.Now,
                    stackTrace = settings.LogStackTrace(type) ? stackTrace : null
                });

                writer.SaveToDisc(logs, path);
            }
        }

        public class LogElement
        {
            public string type;
            public string message;
            public DateTime time;
            public string stackTrace;
        }
    }
}