﻿using UnityEngine;
using System.Collections;

public class LogTester : MonoBehaviour {

    private bool running = false;

	// Use this for initialization
	void Start ()
    {
        Debug.Log("started");
        StartCoroutine(LogRoutine());
    }

    void OnDestroy()
    {
        running = false;
    }
	
	
    IEnumerator LogRoutine()
    {
        running = true;
        while (running)
        {
            int i = Random.Range(0, testLogs.Length);
            string log = testLogs[i];

            float rnd = Random.Range(0f, 1f);

            if (rnd < .4f)
            {
                Debug.Log(log);
            }
            else if(rnd >= .4f && rnd < .8f)
            {
                Debug.LogWarning(log);
            }
            else if (rnd >= .8f && rnd < .9f)
            {
                Debug.LogException(new System.Exception());
            }
            else
            {
                Debug.LogError(log);
            }
            yield return new WaitForSeconds(Random.Range(.1f, 2f));
        }
    }

    public string[] testLogs = new string[]
    {
        "test log 1",
        "hello unity",
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
    };
}
