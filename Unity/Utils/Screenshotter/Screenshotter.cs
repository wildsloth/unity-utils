﻿using UnityEngine;
using System.Collections;
using System;

namespace OpenUtils.Utils
{
    public class Screenshotter : AbstractScreenshotter
    {
        public int resWidth = 2550;
        public int resHeight = 3300;

        private bool takeHiResShot = false;

        private Camera maincam
        {
            get
            {
                return Camera.main;
            }
        }

        void LateUpdate()
        {
            if (takeHiResShot)
            {
                RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
                maincam.targetTexture = rt;
                Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
                maincam.Render();
                RenderTexture.active = rt;
                screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
                maincam.targetTexture = null;
                RenderTexture.active = null; // JC: added to avoid errors
                Destroy(rt);
                byte[] bytes = screenShot.EncodeToPNG();
                string filename = fullPath;
                System.IO.File.WriteAllBytes(filename, bytes);
                Debug.Log(string.Format("Took screenshot to: {0}", filename));
                takeHiResShot = false;
            }
        }

        public override void TakeScreenshot()
        {
            takeHiResShot = true;
        }
    }
}
