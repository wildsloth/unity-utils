﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace OpenUtils.Utils
{
    public class SimpleScreenshotter : AbstractScreenshotter
    {
        [Range(1, 10)]
        public int factor = 1;

        public override void TakeScreenshot()
        {
            string fullpath = Path.Combine(directory, fileName + "-" + DateTime.Now.Timestamp() + ".png");
            Application.CaptureScreenshot(fullpath, factor);
            Debug.Log("save screenshot to " + fullpath);
        }
    }

}
