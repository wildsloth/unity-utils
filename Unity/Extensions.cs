﻿using UnityEngine;
using System.Collections;
using System;

namespace OpenUtils
{
    public static class ExtensionMethods
    {
        public static string Timestamp(this DateTime time)
        {
            return time.ToString("yyyyMMdd-HHmm");
        }

        public static string TimestampYearMonthDay(this DateTime time)
        {
            return time.ToString("yyyyMMdd");
        }
    }



    public static class LogTypExtensions
    {
        public static string ToCleanString(this LogType type)
        {
            switch (type)
            {
                case LogType.Log: return "Log";
                case LogType.Warning: return "Warning";
                case LogType.Error: return "Error";
                case LogType.Exception: return "Exception";
                case LogType.Assert: return "Assert";
                default: return "undefined";
            }
        }
    }

}
