﻿using UnityEngine;
using System.Collections;

using OpenUtils.Json;

public class MyJsonHandler : JsonFile<MyJsonFile>
{
    void Start()
    {
        Debug.Log("json file path is " + absolutePath);
    }

}

public class MyJsonFile
{
    public string myString;
    public ColorJson myStringColor;
    public ColorJson backgroundColor;
}