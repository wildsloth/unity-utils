﻿using UnityEngine;
using System.Collections;
using OpenUtils.Json;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextListener : MonoBehaviour
{
    public MyJsonHandler myJsonFileComponent;

    void OnEnable()
    {
        if (myJsonFileComponent != null)
            myJsonFileComponent.Updated += OnJsonChanged;
    }

    void Start()
    {
        if(myJsonFileComponent != null)
        {
            MyJsonFile file = myJsonFileComponent.result;
            UpdateText(file);
        }
    }

    private void OnJsonChanged(object sender, FileChangedArgs<MyJsonFile> e)
    {
        MyJsonFile json = e.preset as MyJsonFile;
        if(json != null)
        {
            UpdateText(json);
        }
    }


    private Text _text;
    public Text text
    {
        get
        {
            if (_text == null)
            {
                _text = GetComponent<Text>();
            }

            return _text;
        }
    }

    private void UpdateText(MyJsonFile json)
    {
        Debug.Log("new text: " + json.myString);
        text.text = json.myString;

        Color newColor = json.myStringColor.color;
        text.CrossFadeColor(newColor, .2f, false, true);
    }
}