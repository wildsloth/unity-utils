﻿namespace OpenUtils.Json
{
    using System.IO;
    using UnityEngine;
    using Newtonsoft.Json;

    public class Reader
    {
        /// <summary>
        /// Create Object from json file
        /// </summary>
        /// <typeparam name="T">Return type</typeparam>
        /// <param name="fullPath">Absolute Path to JSON File</param>
        /// <returns>Object of type T</returns>
        public T Read<T>(string fullPath)
        {
            try
            {
                using (StreamReader r = new StreamReader(fullPath))
                {
                    // construct json object from json
                    string json = r.ReadToEnd();
                    return Deserialize<T>(json);
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError(e);
                return default(T);
            }
        }

        public T Deserialize<T>(string json)
        {
            T result = JsonConvert.DeserializeObject<T>(json);
            JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings()
            {
                Error = delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                {
                    Debug.LogError(args.CurrentObject + "\n" + args.ErrorContext.Error.Message);
                    args.ErrorContext.Handled = true;
                }
            });
            return result;
        }
    }
}