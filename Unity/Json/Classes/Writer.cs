﻿namespace OpenUtils.Json
{
    using Newtonsoft.Json;
    using System.IO;

    public class Writer
    {
        public JsonSerializerSettings settings;

        public Writer() { }
        public Writer(JsonSerializerSettings settings)
        {
            this.settings = settings;
        }

        public string ToJson(object obj)
        {
            if (settings != null)
                return JsonConvert.SerializeObject(obj, Formatting.None, settings);
            else
                return JsonConvert.SerializeObject(obj, Formatting.None);
        }

        public void SaveToDisc(object obj, string absolutePath)
        {
            string path = Path.GetDirectoryName(absolutePath);

            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }

            File.WriteAllText(absolutePath, ToJson(obj));
        }
    }

}
