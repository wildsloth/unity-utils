﻿namespace OpenUtils
{
    /// <summary>
    /// Parses string into different format
    /// </summary>
    /// <typeparam name="T">output format</typeparam>
    public interface IConverter<T>
    {
        T lastResult { get; }
        T Parse(string str);
    }

    /// <summary>
    /// Convert to different format
    /// </summary>
    /// <typeparam name="T">Source Type</typeparam>
    /// <typeparam name="R">Result Type</typeparam>
    public interface IConverter<T, R>
    {
        R Get(T source);
    }
}